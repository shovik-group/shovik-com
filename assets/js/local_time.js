(function() {
  const utcTimeSpans = document.querySelectorAll('.utc-span')
  const options = { month: 'long', day: 'numeric', year: 'numeric' }

  utcTimeSpans.forEach(utcTimeSpan => {
    console.log("Found date: ", utcTimeSpan.innerText);
    const date = new Date(utcTimeSpan.innerText);
    utcTimeSpan.innerText = date.toLocaleDateString('en-US', options)
  });
})();
