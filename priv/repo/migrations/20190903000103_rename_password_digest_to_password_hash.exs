defmodule ShovikCom.Repo.Migrations.RenamePasswordDigestToPasswordHash do
  use Ecto.Migration

  def change do
    rename table("users"), :password_digest, to: :password_hash
  end
end
