import Config

config :shovik_com, ShovikComWeb.Endpoint,
  server: true

config :shovik_com, ShovikCom.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("DB_HOST"),
  database: System.get_env("DB_NAME"),
  username: System.get_env("DB_USER"),
  password: System.get_env("DB_PASSWORD"),
  port: System.get_env("DB_PORT"),
  ssl: true,
  pool_size: 3

config :shovik_com, ShovikComWeb.Endpoint,
  secret_key_base: System.get_env("SECRET_KEY_BASE")
