use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :shovik_com, ShovikComWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :shovik_com, ShovikCom.Repo,
  username: "alexk",
  password: "",
  database: "shovik_com_test",
  hostname: "127.0.0.1",
  pool: Ecto.Adapters.SQL.Sandbox

config :pbkdf2_elixir, :rounds, 1
