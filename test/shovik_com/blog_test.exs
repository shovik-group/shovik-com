defmodule ShovikCom.BlogTest do
  use ShovikCom.DataCase

  alias ShovikCom.Blog

  describe "posts" do
    alias ShovikCom.Blog.Post

    @valid_attrs %{body: "some body", preview: "some preview", title: "some title", url: "some url"}
    @update_attrs %{body: "some updated body", preview: "some updated preview", title: "some updated title", url: "some updated url"}
    @invalid_attrs %{body: nil, preview: nil, title: nil, url: nil}

    test "list_posts/0 returns all posts" do
      post = insert(:post)
      assert Blog.list_posts(nil) |> Repo.all == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = insert(:post)
      assert Blog.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      author = insert(:user)

      assert {:ok, %Post{} = post} = Blog.create_post(author, @valid_attrs)
      assert post.body == "some body"
      assert post.preview == "some preview"
      assert post.title == "some title"
      assert post.url == "some url"
    end

    test "create_post/1 with invalid data returns error changeset" do
      author = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Blog.create_post(author, @invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = insert(:post)
      assert {:ok, %Post{} = post} = Blog.update_post(post, @update_attrs)
      assert post.body == "some updated body"
      assert post.preview == "some updated preview"
      assert post.title == "some updated title"
      assert post.url == "some updated url"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = insert(:post)
      assert {:error, %Ecto.Changeset{}} = Blog.update_post(post, @invalid_attrs)
      assert post == Blog.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = insert(:post)
      assert {:ok, %Post{}} = Blog.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Blog.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = insert(:post)
      assert %Ecto.Changeset{} = Blog.change_post(post)
    end
  end

  describe "post_images" do
    alias ShovikCom.Blog.PostImage

    @valid_attrs %{primary: true}
    @update_attrs %{primary: false}
    @invalid_attrs %{primary: nil}

    def post_image_fixture(attrs \\ %{}) do
      {:ok, post_image} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Blog.create_post_image()

      post_image
    end

    test "list_post_images/0 returns all post_images" do
      post_image = post_image_fixture()
      assert Blog.list_post_images() == [post_image]
    end

    test "get_post_image!/1 returns the post_image with given id" do
      post_image = post_image_fixture()
      assert Blog.get_post_image!(post_image.id) == post_image
    end

    test "create_post_image/1 with valid data creates a post_image" do
      assert {:ok, %PostImage{} = post_image} = Blog.create_post_image(@valid_attrs)
      assert post_image.primary == true
    end

    test "create_post_image/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Blog.create_post_image(@invalid_attrs)
    end

    test "update_post_image/2 with valid data updates the post_image" do
      post_image = post_image_fixture()
      assert {:ok, %PostImage{} = post_image} = Blog.update_post_image(post_image, @update_attrs)
      assert post_image.primary == false
    end

    test "update_post_image/2 with invalid data returns error changeset" do
      post_image = post_image_fixture()
      assert {:error, %Ecto.Changeset{}} = Blog.update_post_image(post_image, @invalid_attrs)
      assert post_image == Blog.get_post_image!(post_image.id)
    end

    test "delete_post_image/1 deletes the post_image" do
      post_image = post_image_fixture()
      assert {:ok, %PostImage{}} = Blog.delete_post_image(post_image)
      assert_raise Ecto.NoResultsError, fn -> Blog.get_post_image!(post_image.id) end
    end

    test "change_post_image/1 returns a post_image changeset" do
      post_image = post_image_fixture()
      assert %Ecto.Changeset{} = Blog.change_post_image(post_image)
    end
  end
end
