defmodule ShovikCom.UsersTest do
  use ShovikCom.DataCase

  alias ShovikCom.Users
  alias ShovikCom.Users.User

  describe "users" do
    alias ShovikCom.Users.User

    @update_attrs %{email: "user@test.com", first_name: "Dva", last_name: "Sukin"}
    @invalid_attrs %{first_name: nil, last_name: nil, password: nil, password_confirmation: nil}
    @password_attrs %{password: "12341234", password_confirmation: "12341234"}

    test "list_users/0 returns all users" do
      user = insert(:user)
      assert Users.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = insert(:user)
      assert Users.get_user!(user.id) == user
    end

    test "get_user/1 returns the user with given id" do
      user = insert(:user)
      assert Users.get_user(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Users.create_user(params_for(:user, @password_attrs))
      assert user.first_name == "Denver"
      assert user.last_name == "Smith"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(params_for(:user, @invalid_attrs))
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)
      assert {:ok, %User{} = user} = Users.update_user(user, @update_attrs)
      assert user.email == "user@test.com"
      assert user.first_name == "Dva"
      assert user.last_name == "Sukin"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs)
      assert user == Users.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Users.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = insert(:user)
      assert %Ecto.Changeset{} = Users.change_user(user)
    end

    test "update_user_password/2 updates password" do
      user = insert(:user)
      assert {:ok, %User{} = user} = Users.update_user_password(user, @password_attrs)
    end

    test "authenticates user by email and password" do
      {:ok, user} = Users.create_user(params_for(:user, @password_attrs))

      assert {:ok, user} = Users.authenticate_by_email_and_password(user.email, "12341234")
      assert {:error, :unauthorized} = Users.authenticate_by_email_and_password(user.email, "12341234a")
      assert {:error, :not_found} = Users.authenticate_by_email_and_password("bad_email@asdasd.com", "")
    end
  end
end
