defmodule ShovikCom.Factory do
  use ExMachina.Ecto, repo: ShovikCom.Repo

  alias ShovikCom.Users.User
  alias ShovikCom.Blog.Post

  def user_factory do
    %User{
      email: sequence(:email, &"email-#{&1}@example.com"),
      first_name: "Denver",
      last_name: "Smith"
    }
  end

  def post_factory do
    %Post{
      title: "Test blog post",
      url: "test-blog-post",
      preview: "About **something** very cool",
      body: "This is the **ENTIRE** post body",
      author: build(:user),
      publish_at: ~U[2017-02-04 13:09:33.000000Z]
    }
  end

  # def article_factory do
  #   title = sequence(:title, &"Use ExMachina! (Part #{&1})")
  #   # derived attribute
  #   slug = MyApp.Article.title_to_slug(title)
  #   %MyApp.Article{
  #     title: title,
  #     slug: slug,
  #     # associations are inserted when you call `insert`
  #     author: build(:user),
  #   }
  # end

  # # derived factory
  # def featured_article_factory do
  #   struct!(
  #     article_factory(),
  #     %{
  #       featured: true,
  #     }
  #   )
  # end

  # def comment_factory do
  #   %MyApp.Comment{
  #     text: "It's great!",
  #     article: build(:article),
  #   }
  # end
end
