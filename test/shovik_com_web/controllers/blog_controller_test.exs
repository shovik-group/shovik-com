defmodule ShovikComWeb.BlogControllerTest do
  use ShovikComWeb.ConnCase

  alias ShovikCom.Repo
  alias ShovikCom.Blog.Post

  describe "public routes" do
    test "GET index (shows only published posts)", %{conn: conn} do
      published_post = insert(:post, title: "Cool blog post")
      unpublished_post = insert(:post, publish_at: nil, title: "WIP something...")

      conn = get(conn, Routes.blog_path(conn, :index))
      assert html_response(conn, 200) =~ published_post.title
      refute html_response(conn, 200) =~ unpublished_post.title
    end

    test "GET show", %{conn: conn} do
      post = insert(:post)
      conn = get(conn, Routes.blog_path(conn, :show, post))
      assert html_response(conn, 200) =~ post.title
    end
  end

  describe "authenticated routes" do
    setup do
      user = insert(:user)
      conn = build_conn()
             |> assign(:current_user, user)

      {:ok, conn: conn}
    end

    test "GET edit", %{conn: conn} do
      post = insert(:post)
      conn = get(conn, Routes.blog_path(conn, :edit, post))
      assert html_response(conn, 200) =~ "Edit #{post.title}"
    end

    test "GET new", %{conn: conn} do
      conn = get(conn, Routes.blog_path(conn, :new))
      assert html_response(conn, 200) =~ "New Post"
    end

    test "POST create (successful)", %{conn: conn} do
      insert(:user) # TODO: remove when actual logged in user is supported.
      post_params = string_params_for(:post, %{publish_at: ""})
      conn = post(conn, Routes.blog_path(conn, :create, %{"post" => post_params}))

      last_post = Repo.one(Post)
      assert get_flash(conn, :info) == "Post created successfully."
      assert redirected_to(conn) =~ "/blog/#{last_post.id}"
    end

    test "POST create (validation failed)", %{conn: conn} do
      insert(:user)

      post_params = string_params_for(:post, %{publish_at: "", title: ""})
      conn = post(conn, Routes.blog_path(conn, :create, %{"post" => post_params}))
      assert html_response(conn, 200) =~ "can&#39;t be blank"
    end

    test "PUT update (successful)", %{conn: conn} do
      post = insert(:post)
      update_post_params = %{"title" => "New title"}
      conn = put(conn, Routes.blog_path(conn, :update, post, %{"post" => update_post_params}))

      assert redirected_to(conn) =~ "/blog/#{post.id}"
      assert get_flash(conn, :info) == "Post updated successfully."

      post = Repo.get!(Post, post.id)
      assert post.title == "New title"
    end

    test "PUT update (validation failed)", %{conn: conn} do
      post = insert(:post)
      update_post_params = %{"title" => ""}
      conn = put(conn, Routes.blog_path(conn, :update, post, %{"post" => update_post_params}))
      assert html_response(conn, 200) =~ "can&#39;t be blank"
    end
  end
end
