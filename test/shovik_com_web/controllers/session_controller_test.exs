defmodule ShovikComWeb.SessionControllerTest do
  use ShovikComWeb.ConnCase

  alias ShovikCom.Users

  @password "12341234"

  test "GET edit", %{conn: conn} do
    conn = get(conn, Routes.session_path(conn, :new))
    assert html_response(conn, 200) =~ "Got password?"
  end

  test "POST create (successful)", %{conn: conn} do
    {:ok, user} = string_params_for(:user, %{password: @password, password_confirmation: @password})
                  |> Users.create_user

    conn = post(conn, Routes.session_path(conn, :create, %{"session" => %{"email" => user.email, "password" => @password}}))

    assert get_flash(conn, :info) == "Welcome back!"
    assert redirected_to(conn) =~ "/"
  end

  test "POST create (user not found)", %{conn: conn} do
    conn = post(conn, Routes.session_path(conn, :create, %{"session" => %{"email" => "fake@test.com", "password" => @password}}))

    assert get_flash(conn, :error) == "Invalid email/password combination"
    assert html_response(conn, 200) =~ "Got password?"
  end

  test "POST create (wrong password)", %{conn: conn} do
    {:ok, user} = string_params_for(:user, %{password: @password, password_confirmation: @password})
                  |> Users.create_user

    conn = post(conn, Routes.session_path(conn, :create, %{"session" => %{"email" => user.email, "password" => "wrong pass"}}))

    assert get_flash(conn, :error) == "Invalid email/password combination"
    assert html_response(conn, 200) =~ "Got password?"
  end

  test "DELETE delete (logout)", %{conn: conn} do
    user = insert(:user)
    conn = delete(conn, Routes.session_path(conn, :delete, user.id))

    assert redirected_to(conn) =~ "/"
  end
end
