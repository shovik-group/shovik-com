defmodule ShovikComWeb.AuthTest do
  use ShovikComWeb.ConnCase

  alias ShovikComWeb.Auth

  describe "init" do
    test "returns options unchanged" do
      assert Auth.init(%{}) == %{}
    end
  end

  describe "call" do
    test "assigns current_user from user_id in session", %{conn: conn} do
      user = insert(:user)
      new_conn =
        conn
        |> Plug.Test.init_test_session(user_id: user.id)

      assert Auth.call(new_conn, %{}) == assign(new_conn, :current_user, user)
    end
  end

  describe "login" do
    test "adds user to session and assigns", %{conn: conn} do
      user = insert(:user)

      new_conn =
        conn
        |> Plug.Test.init_test_session(user_id: user.id)

      after_conn = Auth.login(new_conn, user)
      assert after_conn.assigns[:current_user] == user
      assert get_session(after_conn, :user_id) == user.id
    end
  end
end
