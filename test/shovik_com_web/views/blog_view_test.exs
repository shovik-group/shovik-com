defmodule ShovikComWeb.BlogViewTest do
  use ShovikComWeb.ConnCase, async: true

  import ShovikComWeb.BlogView

  setup do
    post = build(:post)

    {:ok, post: post}
  end

  describe "post_preview" do
    test "renders markdown preview of post", %{post: post} do
      assert post_preview(post) == {:safe, "<p>About <strong>something</strong> very cool</p>\n"}
    end
  end

  describe "post_edit_button" do
    test "renders post edit button", %{post: post} do
      user = build(:user)
      conn = build_conn()
             |> assign(:current_user, user)
      assert post_edit_button(conn, post) == {:safe, [60, "div", [[32, "class", 61, 34, "level-right", 34]], 62, [60, "div", [[32, "class", 61, 34, "level-item", 34]], 62, [60, "a", [[32, "class", 61, 34, "button is-success", 34], [32, "href", 61, 34, "/blog/-test-blog-post/edit", 34]], 62, "Edit", 60, 47, "a", 62], 60, 47, "div", 62], 60, 47, "div", 62]}
    end
  end

  describe "post_body" do
    test "renders markdown of the post body", %{post: post} do
      assert post_body(post) == {:safe, "<p>This is the <strong>ENTIRE</strong> post body</p>\n"}
    end
  end

  describe "post_url" do
    test "prepared friendly post url", %{post: post} do
      conn = build_conn()
      assert post_url(conn, post) == "/blog/#{post.id}-#{post.url}"
    end
  end

  describe "post_publish_at" do
    test "publish_at display", %{post: post} do
      assert post_publish_at(post) == "2017-02-04T13:09:33.000000Z"
    end
  end
end
