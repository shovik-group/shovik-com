defmodule ShovikComWeb.LayoutViewTest do
  use ShovikComWeb.ConnCase, async: true

  import Phoenix.HTML

  test "renders active menu link", %{conn: conn} do
    conn_with_menu = assign(conn, :menu, "Blog")
    assert safe_to_string(ShovikComWeb.LayoutView.render_menu_item(conn_with_menu, "/", "Blog")) == "<li class=\"is-active\"><a href=\"/\">Blog</a></li>"
  end

  test "renders inactive menu link", %{conn: conn} do
    conn_with_other_menu = assign(conn, :menu, "Home")
    assert safe_to_string(ShovikComWeb.LayoutView.render_menu_item(conn_with_other_menu, "/", "Blog")) == "<li class=\"\"><a href=\"/\">Blog</a></li>"
  end

  test "renders with data-method", %{conn: conn} do
    assert safe_to_string(ShovikComWeb.LayoutView.render_menu_item(conn, "/", "Blog", method: :delete)) =~ "data-method=\"delete\""
  end
end
