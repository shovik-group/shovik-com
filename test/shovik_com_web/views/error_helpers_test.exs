defmodule ShovikComWeb.ErrorHelpersTest do
  use ShovikComWeb.ConnCase, async: true

  import ShovikComWeb.ErrorHelpers
  import Phoenix.HTML

  test "renders error tag" do
    form = %{errors: [email: {"Cannot be blank", []}]}
    assert safe_to_string(error_tag(form, :email) |> hd) == "<p class=\"help is-danger\">Cannot be blank</p>"
  end
end
