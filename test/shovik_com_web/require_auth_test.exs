defmodule ShovikComWeb.RequireAuthTest do
  use ShovikComWeb.ConnCase

  # import Phoenix.Controller, only: [fetch_flash: 2]
  # import Plug.Conn, only: [fetch_session: 2]

  alias ShovikComWeb.RequireAuth

  describe "init" do
    test "returns options unchanged", %{conn: _conn} do
      assert RequireAuth.init(%{}) == %{}
    end
  end

  describe "call" do
    test "returns call unchanged if user is authenticated", %{conn: conn} do
      user = build(:user)
      new_conn = assign(conn, :current_user, user)
      assert RequireAuth.call(new_conn, %{}) == new_conn
    end

    test "redirects to home index if not authenticated", %{conn: conn} do
      new_conn =
        conn
        |> Plug.Test.init_test_session(%{})
        |> fetch_session
        |> fetch_flash
        |> RequireAuth.call(%{})

      assert redirected_to(new_conn) == "/"
    end
  end
end
