# RUN COMMAND FOR DEVELOPMENT:
# docker run -it --rm -e SECRET_KEY_BASE=$SHOVIK_SECRET_KEY_BASE -e DB_HOST=$SHOVIK_DB_HOST -e DB_NAME=$SHOVIK_DB_NAME -e DB_USER=$SHOVIK_DB_USER -e DB_PASSWORD=$SHOVIK_DB_PASSWORD -p 80:80 shovik-com:latest

#================================================================================
# BUILD IMAGE
#================================================================================
FROM elixir:1.9.4 as build

ENV HOME=/app
ENV MIX_ENV=prod SHELL=/bin/bash

RUN mix local.hex --force && \
    mix local.rebar --force

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get install -y nodejs

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && apt-get install yarn

WORKDIR /app

# Cache elixir deps.
COPY mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

# Cache node deps.
RUN mkdir assets
COPY assets/package.json assets/yarn.lock ./assets/
RUN cd assets && yarn

COPY . .

# Build and digest static assets
RUN cd assets && ./node_modules/webpack/bin/webpack.js --mode production && \
    cd .. && mix phx.digest

RUN mix release

#================================================================================
# RUN IMAGE
#================================================================================
FROM elixir:1.9.4 as run

ENV HOME=/app
ENV MIX_ENV=prod SHELL=/bin/bash

ARG COMMIT_HASH
ARG COMMIT_HASH_LONG

ENV COMMIT_HASH=${COMMIT_HASH}
ENV COMMIT_HASH_LONG=${COMMIT_HASH_LONG}

WORKDIR /app

COPY --from=build /app/_build/prod/rel/shovik_com .

# CMD ["/app/bin/shovik_com", "start"]
CMD trap exit TERM; bin/shovik_com start & wait
