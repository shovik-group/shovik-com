defmodule ShovikComWeb.BlogView do
  use ShovikComWeb, :view

  alias ShovikCom.Blog.Post

  def post_url(conn, post, action \\ :show) do
    if post do
      Routes.blog_path(conn, action, "#{post.id}-#{post.url}")
    else
      Routes.blog_path(conn, :index)
    end
  end

  def post_edit_button(conn, post) do
    if conn.assigns[:current_user] do
      content_tag :div, class: "level-right" do
        content_tag :div, class: "level-item" do
          link "Edit", to: post_url(conn, post, :edit), class: "button is-success"
        end
      end
    else
      ""
    end
  end

  def post_preview(post) do
    post.preview
    |> markdown
  end

  def post_body(post) do
    post.body
    |> markdown
  end

  defp markdown(body) do
    body
    |> Earmark.as_html!(%Earmark.Options{breaks: true})
    |> raw
  end

  def post_publish_at(%Post{publish_at: nil}), do: ""
  def post_publish_at(%Post{publish_at: publish_at}) do
    publish_at
    |> DateTime.to_iso8601
  end
end
