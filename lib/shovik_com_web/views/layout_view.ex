defmodule ShovikComWeb.LayoutView do
  use ShovikComWeb, :view

  def render_menu_item(conn, path, title, options \\ []) do
    li_class = if conn.assigns[:menu] == title, do: "is-active", else: ""

    content_tag :li, class: li_class do
      link title, Keyword.merge(options, to: path)
    end
  end
end
