defmodule ShovikComWeb.Auth do
  @moduledoc """
  Loads current user from session and assigns the loaded %User{} to conn.
  """

  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    if conn.assigns[:current_user] do
      conn
    else
      user_id = get_session(conn, :user_id)
      user = user_id && ShovikCom.Users.get_user(user_id)

      assign(conn, :current_user, user)
    end
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end
end
