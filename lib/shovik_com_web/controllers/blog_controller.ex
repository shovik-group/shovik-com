defmodule ShovikComWeb.BlogController do
  use ShovikComWeb, :controller

  alias ShovikCom.Blog
  alias ShovikCom.Blog.Post
  alias ShovikCom.Repo

  plug :put_layout, :app
  plug :load_post when action in [:show, :edit, :update]
  plug ShovikComWeb.RequireAuth when action in [:new, :edit, :create, :edit, :update]

  def index(conn, _params) do
    posts = conn.assigns[:current_user]
            |> Blog.list_posts
            |> Repo.all

    render(conn, "index.html", menu: "Blog", posts: posts)
  end

  def show(conn, _params) do
    render(conn, "show.html", menu: "Blog")
  end

  def new(conn, _params) do
    changeset = Post.changeset(%Post{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    author = conn.assigns.current_user

    case Blog.create_post(author, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: Routes.blog_path(conn, :show, post))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, _params) do
    changeset = Post.changeset(conn.assigns[:post])
    render(conn, "edit.html", menu: "Blog", changeset: changeset)
  end

  def update(conn, %{"post" => post_params}) do
    post = conn.assigns[:post]

    case Blog.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: Routes.blog_path(conn, :show, post))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> render("edit.html", post: post, changeset: changeset)
    end
  end

  defp load_post(conn, _) do
    id = conn.params["id"] |> String.split("-") |> hd
    post = Blog.get_post!(id, conn.assigns[:current_user])

    assign(conn, :post, post)
  end
end
