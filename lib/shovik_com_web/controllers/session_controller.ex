defmodule ShovikComWeb.SessionController do
  use ShovikComWeb, :controller

  def new(conn, _) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"email" => email, "password" => password}}) do
    case ShovikCom.Users.authenticate_by_email_and_password(email, password) do
      {:ok, user} ->
        conn
        |> ShovikComWeb.Auth.login(user)
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: Routes.home_path(conn, :index))

      {:error, _reason} ->
        conn
        |> put_flash(:error, "Invalid email/password combination")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> ShovikComWeb.Auth.logout
    |> redirect(to: Routes.home_path(conn, :index))
  end
end
