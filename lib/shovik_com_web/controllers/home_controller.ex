defmodule ShovikComWeb.HomeController do
  use ShovikComWeb, :controller

  plug :put_layout, :home

  def index(conn, _params) do
    render(conn, "index.html", menu: "Home")
  end
end
