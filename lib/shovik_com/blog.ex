defmodule ShovikCom.Blog do
  @moduledoc """
  The Blog context.
  """

  import Ecto.Query, warn: false
  import Ecto.Changeset
  alias ShovikCom.Repo

  alias ShovikCom.Blog.Post

  @doc """
  Returns the list of posts:
    - if user is nil, then only published ones.
    - if user is not nil, then ALL posts.

  ## Examples

      iex> list_posts()
      [%Post{}, ...]

  """
  def list_posts(nil) do
    list_all_posts()
    |> where([p], not is_nil(p.publish_at))
  end
  def list_posts(_current_user), do: list_all_posts()

  @doc """
  Returns list of ALL posts.
  """
  def list_all_posts do
    from p in Post,
      order_by: [desc: p.publish_at],
      preload: [:author]
  end

  @doc """
  Gets a single post.

  Raises `Ecto.NoResultsError` if the Post does not exist.

  ## Examples

      iex> get_post!(123)
      %Post{}

      iex> get_post!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post!(id, current_user \\ nil) do
    current_user
    |> list_posts
    |> where([p], p.id == ^id)
    |> Repo.one!
    |> Repo.preload(:author)
  end

  @doc """
  Creates a post.

  ## Examples

      iex> create_post(%{field: value})
      {:ok, %Post{}}

      iex> create_post(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post(author, attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> put_assoc(:author, author, required: true)
    |> Repo.insert()
  end

  @doc """
  Updates a post.

  ## Examples

      iex> update_post(post, %{field: new_value})
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Post.

  ## Examples

      iex> delete_post(post)
      {:ok, %Post{}}

      iex> delete_post(post)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(%Post{} = post) do
    Repo.delete(post)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{source: %Post{}}

  """
  def change_post(%Post{} = post) do
    Post.changeset(post, %{})
  end

  alias ShovikCom.Blog.PostImage

  @doc """
  Returns the list of post_images.

  ## Examples

      iex> list_post_images()
      [%PostImage{}, ...]

  """
  def list_post_images do
    Repo.all(PostImage)
  end

  @doc """
  Gets a single post_image.

  Raises `Ecto.NoResultsError` if the Post image does not exist.

  ## Examples

      iex> get_post_image!(123)
      %PostImage{}

      iex> get_post_image!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post_image!(id), do: Repo.get!(PostImage, id)

  @doc """
  Creates a post_image.

  ## Examples

      iex> create_post_image(%{field: value})
      {:ok, %PostImage{}}

      iex> create_post_image(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post_image(attrs \\ %{}) do
    %PostImage{}
    |> PostImage.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a post_image.

  ## Examples

      iex> update_post_image(post_image, %{field: new_value})
      {:ok, %PostImage{}}

      iex> update_post_image(post_image, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post_image(%PostImage{} = post_image, attrs) do
    post_image
    |> PostImage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PostImage.

  ## Examples

      iex> delete_post_image(post_image)
      {:ok, %PostImage{}}

      iex> delete_post_image(post_image)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post_image(%PostImage{} = post_image) do
    Repo.delete(post_image)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post_image changes.

  ## Examples

      iex> change_post_image(post_image)
      %Ecto.Changeset{source: %PostImage{}}

  """
  def change_post_image(%PostImage{} = post_image) do
    PostImage.changeset(post_image, %{})
  end
end
