defmodule ShovikCom.Users.User do
  @moduledoc """
  Represent a user record.
  """

  use Ecto.Schema

  import Ecto.Changeset

  alias ShovikCom.Blog.Post

  schema "users" do
    has_many :posts, Post, foreign_key: :author_id

    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :password_hash, :string

    timestamps()

    # Virtual fields
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
  end

  @doc false
  def insert_changeset(user, params) do
    user
    |> changeset(params)
    |> cast(params, [:password, :password_confirmation])
    |> validate_password
    |> put_password_hash
  end

  @doc false
  def changeset(user, params) do
    user
    |> cast(params, [:email, :first_name, :last_name])
    |> validate_required([:email, :first_name, :last_name])
  end

  def update_password_changeset(user, params) do
    user
    |> cast(params, [:password, :password_confirmation])
    |> validate_password
    |> put_password_hash
  end

  defp validate_password(changeset) do
    changeset
    |> validate_required([:password, :password_confirmation])
    |> validate_required(:password)
    |> validate_length(:password, min: 8, max: 100)
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Pbkdf2.hash_pwd_salt(password))
      _ -> changeset
    end
  end
end
