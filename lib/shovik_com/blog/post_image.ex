defmodule ShovikCom.Blog.PostImage do
  @moduledoc """
  Represents a blog post image record.
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias ShovikCom.Blog.Post

  schema "post_images" do
    belongs_to :post, Post

    field :picture, ShovikCom.Uploaders.Picture.Type
    field :primary, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(post_image, attrs) do
    post_image
    |> cast(attrs, [:primary])
    |> validate_required([:primary])
  end
end
