defmodule ShovikCom.Blog.Post do
  @moduledoc """
  Represents a blog post record
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias ShovikCom.Blog.PostImage
  alias ShovikCom.Users.User

  schema "posts" do
    belongs_to :author, User
    has_many :post_images, PostImage
    has_one :primary_image, PostImage

    field :title, :string
    field :url, :string
    field :preview, :string
    field :body, :string
    field :publish_at, :utc_datetime_usec

    timestamps()
  end

  @doc false
  def changeset(post, params \\ %{}) do
    post
    |> cast(params, [:title, :url, :preview, :body, :publish_at])
    |> validate_required([:title, :url, :preview, :body])
  end
end
