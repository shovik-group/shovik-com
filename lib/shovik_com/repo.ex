defmodule ShovikCom.Repo do
  use Ecto.Repo,
    otp_app: :shovik_com,
    adapter: Ecto.Adapters.Postgres
end
